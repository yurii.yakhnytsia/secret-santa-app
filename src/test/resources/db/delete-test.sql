insert into members (id, name, email) values
    (1, 'Gaal Dornic', 'dornic@foundation.com'),
    (2, 'Hari Seldon', 'seldon@foundation.com'),
    (3, 'Salvor Hardin', 'hardin@foundation.com'),
    (4, 'Daneel Olivan', 'olivan@foundation.com');

update members set receiver_id = 1 where id = 3;
update members set receiver_id = 2 where id = 4;
update members set receiver_id = 3 where id = 1;
update members set receiver_id = 4 where id = 2;