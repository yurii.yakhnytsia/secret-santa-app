package com.projects.secretsanta.secretsanta.servcie.impl;

import com.projects.secretsanta.secretsanta.dto.MemberDto;
import com.projects.secretsanta.secretsanta.repository.MemberRepository;
import com.projects.secretsanta.secretsanta.servcie.SecretSantaService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.jdbc.Sql;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class DefaultSecretSantaServiceTest {


    private SecretSantaService secretSantaService;

    @Autowired
    private MemberRepository memberRepository;

    @BeforeAll
    void initService() {
        secretSantaService = new DefaultSecretSantaService(memberRepository);
    }

    @Test
    void addMember() {
        MemberDto newMember = new MemberDto("Santa", "santa@mail.com");
        newMember = secretSantaService.addMember(newMember);
        assertNotNull(newMember.getId());
        List<MemberDto> allMembers = secretSantaService.getAllMembers();
        assertEquals(1,allMembers.size());
    }

    @Test
    @Sql(scripts = "/db/delete-test.sql")
    void removeMember() {
        List<MemberDto> members = secretSantaService.getAllMembers();
        assertEquals(4,members.size());
        members = secretSantaService.removeMember(4L);
        assertEquals(3,members.size());

    }

    @Test
    @Sql(scripts = "/db/randomize-test.sql")
    void randomizeAndGetResult() {
        List<MemberDto> members = secretSantaService.randomizeAndGetResult();
        members.forEach(memberDto -> {
            assertNotNull(memberDto.getReceiver());
            assertNotEquals(memberDto.getId(), memberDto.getReceiver().getId());
        });
    }
}