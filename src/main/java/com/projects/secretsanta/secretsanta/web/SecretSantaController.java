package com.projects.secretsanta.secretsanta.web;


import com.projects.secretsanta.secretsanta.dto.MemberDto;
import com.projects.secretsanta.secretsanta.servcie.SecretSantaService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/api/members")
@Slf4j
@CrossOrigin
public class SecretSantaController {

    private final SecretSantaService secretSantaService;

    public SecretSantaController(SecretSantaService secretSantaService) {
        this.secretSantaService = secretSantaService;
    }

    @GetMapping
    public List<MemberDto> getMembers() {
        return secretSantaService.getAllMembers();
    }

    @PostMapping
    public MemberDto addNewMember(@RequestBody MemberDto memberDto) {
        return secretSantaService.addMember(memberDto);
    }

    @PutMapping("/{memberId}")
    public MemberDto editMember(@PathVariable("memberId") Long memberId, @RequestBody MemberDto memberDto) {
        return secretSantaService.updateMember(memberDto, memberId);
    }

    @DeleteMapping("/{memberId}")
    public List<MemberDto> deleteMember(@PathVariable("memberId") Long memberId) {
        return secretSantaService.removeMember(memberId);
    }

    @PostMapping("/randomize")
    public List<MemberDto> randomizeAndGetResult() {
        return secretSantaService.randomizeAndGetResult();
    }
}
