package com.projects.secretsanta.secretsanta.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity()
@Table(name = "members")
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Member {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    @EqualsAndHashCode.Include
    private String name;

    @Column(name = "email")
    @EqualsAndHashCode.Include
    private String email;

    @OneToOne(cascade = {CascadeType.PERSIST})
    @JoinColumn(name = "receiver_id")
    @ToString.Exclude
    private Member giftReceiver;
}
