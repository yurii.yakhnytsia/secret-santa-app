package com.projects.secretsanta.secretsanta.servcie;

import com.projects.secretsanta.secretsanta.dto.MemberDto;

import java.util.List;

public interface SecretSantaService {

    MemberDto addMember(MemberDto memberDto);

    MemberDto updateMember(MemberDto memberDto, Long memberId);

    List<MemberDto> removeMember(Long memberId);

    List<MemberDto> getAllMembers();

    List<MemberDto> randomizeAndGetResult();

}
