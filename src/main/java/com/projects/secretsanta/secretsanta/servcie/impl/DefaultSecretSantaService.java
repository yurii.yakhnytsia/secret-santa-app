package com.projects.secretsanta.secretsanta.servcie.impl;

import com.projects.secretsanta.secretsanta.domain.Member;
import com.projects.secretsanta.secretsanta.dto.MemberDto;
import com.projects.secretsanta.secretsanta.exception.MemberNotFoundException;
import com.projects.secretsanta.secretsanta.repository.MemberRepository;
import com.projects.secretsanta.secretsanta.servcie.SecretSantaService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
public class DefaultSecretSantaService implements SecretSantaService {

    private final MemberRepository memberRepository;

    public DefaultSecretSantaService(MemberRepository memberRepository) {
        this.memberRepository = memberRepository;
    }

    @Override
    public MemberDto addMember(MemberDto memberDto) {
        Member newMember = new Member();
        newMember.setEmail(memberDto.getEmail());
        newMember.setName(memberDto.getName());
        memberRepository.save(newMember);
        return mapToMemberDto(newMember);
    }

    @Override
    public MemberDto updateMember(MemberDto memberDto, Long memberId) {
        Member memberFromDb = memberRepository.findById(memberId)
                .orElseThrow(() -> new MemberNotFoundException("Member with id: " + memberId + " not found."));
        memberFromDb.setName(memberDto.getName());
        memberFromDb.setEmail(memberDto.getEmail());
        memberFromDb = memberRepository.save(memberFromDb);
        return mapToMemberDto(memberFromDb);
    }

    @Override
    public List<MemberDto> removeMember(Long memberId) {
        Member memberFromDb = memberRepository.findById(memberId)
                .orElseThrow(() -> new MemberNotFoundException("Member with id: " + memberId + " not found."));
        memberRepository.findByGiftReceiverId(memberFromDb.getId())
                .ifPresent(
                        giftPresenter -> {
                            giftPresenter.setGiftReceiver(null);
                            memberRepository.saveAndFlush(giftPresenter);
                        }
                );
        memberRepository.delete(memberFromDb);
        memberRepository.flush();
        return getAllMembers();
    }

    @Override
    public List<MemberDto> getAllMembers() {
        List<Member> membersFromDb = memberRepository.findAll();
        return mapToMemberDtoList(membersFromDb);
    }

    @Override
    public List<MemberDto> randomizeAndGetResult() {
        List<Member> allMembers = memberRepository.findAll();
        if (allMembers.isEmpty()) {
            return Collections.emptyList();
        }
        Collections.shuffle(allMembers);
        Iterator<Member> memberIterator = allMembers.iterator();
        Member first = memberIterator.next();
        while (memberIterator.hasNext()) {
            Member next = memberIterator.next();
            first.setGiftReceiver(next);
            first = next;
        }
        Member last = allMembers.get(allMembers.size() - 1);
        last.setGiftReceiver(allMembers.get(0));
        allMembers = memberRepository.saveAll(allMembers);
        allMembers.sort(Comparator.comparing(Member::getId));
        return mapToMemberDtoList(allMembers);
    }

    private MemberDto mapToMemberDto(Member member) {
        Member giftReceiver = member.getGiftReceiver();
        MemberDto giftReceiverDto = giftReceiver == null ? null :
                new MemberDto(giftReceiver.getId(), giftReceiver.getEmail(), giftReceiver.getName(), null);
        return new MemberDto(member.getId(), member.getEmail(), member.getName(), giftReceiverDto);
    }

    private List<MemberDto> mapToMemberDtoList(Collection<Member> memberCollection) {
        return memberCollection.stream()
                .map(this::mapToMemberDto)
                .collect(Collectors.toList());
    }
}
