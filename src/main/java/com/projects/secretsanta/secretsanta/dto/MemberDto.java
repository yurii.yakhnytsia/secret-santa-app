package com.projects.secretsanta.secretsanta.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MemberDto {
    private Long id;
    private String email;
    private String name;
    private MemberDto receiver;

    public MemberDto(String email, String name) {
        this.email = email;
        this.name = name;
    }
}
